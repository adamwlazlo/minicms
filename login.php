<?php
	session_start();

    $login = $_POST['login'];
    $password = $_POST['password'];

    $file = @fopen("acces", "r"); //otwarcie pliku do odczytu

    if ($file)
    {
        $line = fgets($file); 
        $data = explode(',', $line);
        if(($login==$data[0])&&(password_verify($password, $data[1])))
        {   
            echo "Zalogowano";
            $_SESSION['logged']=true; //po poprawnym logowaniu tworzymy zmienna 'logged' i ustawiamy jej wartosc na true, do pozniejszego sprawdzania czy ktos jest już zalogowany
            $_SESSION['login']=$data[0];
            unset($_SESSION['acc_error']);
            unset($_SESSION['log_error']); //jesli logowanie było poprawn usuwamy zmienne przechowujace bledy
            header('Location: index.php');
        }
        else
        {
            $_SESSION['acc_error']='<span>Nieprawidłowe dane</span>';
            header('Location: admin/index.php');
        }
    }
    else
    {
        $_SESSION['acc_error']='<span>Access error</span>';
        header('Location: admin/index.php');
    }

    fclose($file);

   
