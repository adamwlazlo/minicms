<?php
	session_start();
?>

<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" charset="utf-8" />
	<title>Stronka z logowaniem</title>
</head>

<body>


<?php
	if( (isset($_SESSION['logged'])) && ($_SESSION['logged']==true) )
	{
		echo "Witaj ".$_SESSION['login']."! <a href='logout.php'>Wyloguj</a>";
	}
?>

<br /><br />
<div>
<h1>Akapit 1</h1>
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet ullamcorper lectus. Nunc a semper ligula. Aenean auctor facilisis risus, et egestas justo venenatis id. Sed vel elit gravida, volutpat ligula in, auctor risus. In sed dui lobortis, varius justo sed, euismod est.</p>
</div>

<?php
	if( (isset($_SESSION['logged'])) && ($_SESSION['logged']==true) )
	{
		echo "<a href='#'>Edytuj</a> <a href='#'>Usuń</a>";
	}
?>

<div>
<h1>Akapit 2</h1>
<p>
Phasellus ultrices dui in tincidunt molestie. Donec maximus elementum ante vitae interdum. Vivamus ante leo, porttitor ut venenatis non, faucibus a augue. Morbi ligula lacus, auctor et metus vel, tincidunt maximus ex. Vivamus pellentesque eleifend tincidunt.</p>
</div>

<?php
	if( (isset($_SESSION['logged'])) && ($_SESSION['logged']==true) )
	{
		echo "<a href='#'>Edytuj</a> <a href='#'>Usuń</a>";
	}
?>

<?php
	if( (isset($_SESSION['logged'])) && ($_SESSION['logged']==true) )
	{
		echo "<br/><br/><a href='#'>Dodaj akapit</a>";
	}
?>

</body>
</html>